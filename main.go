//This file is part of gobitmex
//
//
//gobitmex is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation version 3 of the License.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
// Copyright (C) 2018 Pierre NN

//Package gobitmex provides a simple go interface to the bitmex WS/HTTP API https://gitlab.com/pierrenn/gobitmex
//
//  - construct order book from L2 to get quick data from bitmex
//
//  - use your own orderbook data structure (see [`OrderBookHandler`](https://godoc.org/gitlab.com/pierrenn/gobitmex#OrderBookHandler) interface) or use the basic one provided as an example
//
//  - get updates of a price level queue after a specific update has been seen (helps you to detect hidden contracts/your order position in the OB queue/...)
//
//  - includes a very simple (not swagger bloated) HTTP API so you can use REST authenticated endpoints with timestamps compatible with the WS authenticated endpoints
package gobitmex

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"sync"
	"sync/atomic"
)

//Client is a bitmex API client
type Client struct {
	logger *log.Logger
	apiURL string    //used base url for API (vary from testnet/real)
	key    *struct { //key is nil if we don't want to authenticate
		public  string
		private string
	}

	streams   []*wsstream //slice of websocket streams
	streamsMu sync.Mutex  //the server or the user can drop a connection stream (streams can be shortened concurently)

	hasClosed   chan struct{}  //non blocking if we requested closing
	streamsWait sync.WaitGroup //used to wait for all websocket streams/reader to close

	Orders chan []Order            //buffered channel (see StartStreaming) for showing orders
	Trades map[string]chan []Trade //map[symbol] of last trades

	isOBEmpty   map[string]bool             //used to know if a given OB is empty
	OBWaitGroup map[string]*sync.WaitGroup  //used to wait for a OB to be filled
	OrderBook   map[string]OrderBookHandler //map[symbol] of an OB data structure implementing the OrderBookHandler interface
}

// NewClient creates an empty client (connects to the testnet if isTestnet)
func NewClient(isTestnet bool) (*Client, error) {
	mexurl := "www.bitmex.com"
	if isTestnet {
		mexurl = "testnet.bitmex.com"
	}

	return &Client{
		apiURL:    mexurl,
		logger:    log.New(os.Stderr, "", log.LstdFlags),
		hasClosed: make(chan struct{}, 1),
	}, nil
}

//Login registers public/private key for a bitmex client
func (c *Client) Login(publicKey, privateKey string) error {
	if c.key != nil {
		return fmt.Errorf("client key already set to [pub_key=%s]", c.key.public)
	}

	c.key = &struct {
		public  string
		private string
	}{
		public:  publicKey,
		private: privateKey,
	}

	return nil
}

//SetLogger sets the library overall logger.
//client.logger is private, otherwise we could modify client.logger without modifiying
//the streams loggers.
func (c *Client) SetLogger(l *log.Logger) {
	c.logger = l

	c.streamsMu.Lock()
	defer c.streamsMu.Unlock()
	for _, w := range c.streams {
		if w != nil {
			w.logger = l
		}
	}
}

//GetLogger gets the library logger
func (c *Client) GetLogger() *log.Logger {
	return c.logger
}

//find the first stream subscribed to a StreamId, return nil if not found
func (c *Client) findStream(b StreamID, symbol string) *wsstream {
	c.streamsMu.Lock()
	defer c.streamsMu.Unlock()
	for _, w := range c.streams {
		if atomic.LoadUint64(&w.subscribedStreams)&uint64(b) != 0 && (symbol == "" || symbol == w.symbol) {
			return w
		}
	}

	return nil
}

//remove a stream from our list. Does not preserve order
//returns true iff an element was found
func (c *Client) removeStream(wss *wsstream) bool {
	c.streamsMu.Lock()
	defer c.streamsMu.Unlock()

	for k, w := range c.streams {
		if w == wss {
			c.streams[k] = c.streams[len(c.streams)-1]
			c.streams = c.streams[:len(c.streams)-1]
			return true
		}
	}

	return false
}

//StartStreaming starts feeds streaming for the streams described by the StreamID bitmask, and
//use a buffered channel of size chanSize for transmitting results (if requested feeds need to).
//Use symbol to register a particular symbol (use "" if no symbol is needed).
func (c *Client) StartStreaming(s StreamID, chanSize int, symbol string) error {
	if chanSize < 1 {
		return fmt.Errorf("Please provide a buffer size for channel >= 1")
	}

	if s&SetOrder != 0 {
		if c.findStream(SetOrder, "") != nil {
			return fmt.Errorf("already found a stream subscribed to order")
		}

		wss, err := c.newOrderStream(chanSize)
		if err != nil {
			return fmt.Errorf("error while creating order stream: %v", err)
		}

		c.streamsMu.Lock()
		if err := wss.startPingTicker(); err != nil {
			wss.Close()
			return fmt.Errorf("could not start order ping/pongs: %v", err)
		}
		c.streams = append(c.streams, wss)
		c.streamsMu.Unlock()

		c.streamsWait.Add(1)
		go c.fillOrdersStream(wss)
	}

	if (s & (SetTrade | SetOrderbookL2)) != 0 {
		var err error
		wss := c.findStream(SetTrade|SetOrderbookL2, symbol)

		if wss == nil { //we need to create a new stream
			wss, err = newStream(c.logger, c.apiURL, chanSize, symbol)
			if err != nil {
				return fmt.Errorf("could not open obtrade stream: %v", err)
			}

			if err := c.subscribeOBTStream(chanSize, s, wss); err != nil {
				return fmt.Errorf("could not subscribe to obtrade: %v", err)
			}

			c.streamsMu.Lock()
			if err := wss.startPingTicker(); err != nil {
				wss.Close()
				return fmt.Errorf("could not start order ping/pongs: %v", err)
			}
			c.streams = append(c.streams, wss)
			c.streamsMu.Unlock()

			c.streamsWait.Add(1)
			go c.fillOBTStream(wss)
		} else {
			if err := c.subscribeOBTStream(chanSize, s, wss); err != nil {
				return fmt.Errorf("could not subscribe to obtrade: %v", err)
			}
		}

	}

	return nil
}

//Close closes the client
func (c *Client) Close() {
	//Close websocket streams (terminate all readers wait)
	c.streamsMu.Lock()
	for _, w := range c.streams {
		if w != nil {
			w.Close()
		}
	}
	c.streamsMu.Unlock()

	//signal to handler functions to stop transmitting data to client
	c.hasClosed <- struct{}{}

	//Close orders channel
	if c.Orders != nil {
		close(c.Orders)
	}

	//Close trade channels
	if c.Trades != nil {
		for _, e := range c.Trades {
			if e != nil {
				close(e)
			}
		}
	}

	//wait for the readers to end
	c.streamsWait.Wait()
}

/**** REST ***/
/*********************/
//base HTTP api url
const apiHTTPUrl = "/api/v1"

//HTTPError helps to encode REST error returned by API
type HTTPError struct {
	Message string `json:"message,omitempty"`
	Name    string `json:"name,omitempty"`
}

//NewRequest creates an http request authorized/non authorized with the right attributes
func (c *Client) NewRequest(method, path string, body map[string]interface{}) (*http.Request, error) {
	requestPath := apiHTTPUrl + path

	//TODO: handle case for GET with a body
	var postBody []byte
	if body != nil {
		var err error
		postBody, err = json.Marshal(body)
		if err != nil {
			return nil, fmt.Errorf("Request could not serialize the body: %v", err)
		}
	}

	req, err := http.NewRequest(method, "https://"+c.apiURL+requestPath, bytes.NewReader(postBody))
	if err != nil {
		return nil, fmt.Errorf("Request could not create request: %v", err)
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-Requested-With", "XMLHttpRequest")

	signatureTimerLock.Lock()
	defer signatureTimerLock.Unlock()

	if c.key != nil {
		expires := generateExpires()
		req.Header.Add("api-key", c.key.public)
		req.Header.Add("api-expires", expires)
		req.Header.Add("api-signature",
			computeSignature(c.key.private, method, requestPath, expires, string(postBody)))
	}

	return req, nil
}
